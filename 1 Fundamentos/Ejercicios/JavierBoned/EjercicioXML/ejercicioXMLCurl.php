<?php
  $url = "https://newtral.es/feed/";

  $page = download_page($url);
  $DOM = new DOMDocument();
  $DOM->loadXML($page);
  $listaItems = $DOM->getElementsByTagName("item");

  foreach ($listaItems as $item) {
    $url = getURL($item);
    $creador = getCreator($item);
    echo "<b>URL:</b> ".$url."<br/><b>Creador:</b> ".$creador."<br/><br/>";
 }

 function getURL($item){
    return $item->getElementsByTagName("link")->item(0)->nodeValue;
 }
 function getCreator($item){
    return $item->getElementsByTagName("creator")->item(0)->nodeValue;
 }

 function download_page($url){
   $ch = curl_init();
   curl_setopt($ch, CURLOPT_URL, $url );
   curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
   $headers = ['Content-type: application/xml'];
   curl_setopt($ch,CURLOPT_HTTPHEADER,$headers);
   $result = curl_exec($ch);
   curl_close($ch);
   return $result;
 }
?>