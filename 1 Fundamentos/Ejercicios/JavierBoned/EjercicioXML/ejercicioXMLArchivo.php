<?php
  $xml = file_get_contents("newtral.xml");
  $DOM = new DOMDocument();
  $DOM->loadXML($xml);
  $listaItems = $DOM->getElementsByTagName("item");

  foreach ($listaItems as $item) {
    $url = getURL($item);
    $creador = getCreator($item);
    echo "<b>URL:</b> ".$url."<br/><b>Creador:</b> ".$creador."<br/><br/>";
 }

 function getURL($item){
    return $item->getElementsByTagName("link")->item(0)->nodeValue;
 }
 function getCreator($item){
    return $item->getElementsByTagName("creator")->item(0)->nodeValue;
 }
?>