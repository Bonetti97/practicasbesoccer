<body>

<?php
    //3-Escribir un programa que saque el área de cualquier cuadrado, círculo o triángulo dados los tamaños de cada lado
    function calcularAreaCuadrado($lado){
        return $lado * $lado;
    }

    function calcularAreaTriangulo($lado){
        return  sqrt(3*$lado) / 2 ;
    }

    function calcularAreaCirculo($radio){
        return $radio * $radio * pi();
    }
?>
    El area del cuadrado es: <?php echo calcularAreaCuadrado($_GET["cuadrado"])?> <br/>
    El area del triangulo es: <?php echo calcularAreaTriangulo($_GET["triangulo"])?> <br/>
    El area del circulo es: <?php echo calcularAreaCirculo($_GET["circulo"])?> <br/>
    <a href="ejercicio3.php">volver</a>
</body>