<?php
    //1-Escribir un programa que imprima por pantalla números del 1 al 100. Cuando el nº sea múltiplo de 3, escribid “Fizz” en lugar del nº, y para múltiplos de 5 escribid “Buzz”. 
    //Para múltiplos de ambos nºs escribid “FizzBuzz”
    for($i=1; $i<=100;$i++){
        $esde3 = 0;
        $esde5 = 0;
        if($i % 3 == 0){
            $esde3=1;
            echo "Fizz";
        }
        if($i % 5 == 0){
            $esde5=1;
            echo "Buzz";
        }
        if ($esde3 == 0 && $esde5 == 0){
            echo $i;
        }
        echo "<br>";
    }

?>
