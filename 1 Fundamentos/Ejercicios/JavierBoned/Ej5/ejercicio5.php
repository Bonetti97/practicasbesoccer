<?php
    //5-Dados dos arrays de nºs aleatorios, imprimir un tercer array que saque la suma de cada una de sus posiciones.
    
    $array1 = array();
    $array2 = array();
    $arraysuma = array();
    
    for($i=0;$i<5;$i++){
        $array1[$i] = rand(1,10);
        $array2[$i] = rand(1,10);
        $arraysuma[$i] = $array1[$i] + $array2[$i];
    }

?>

<div>
    <p>El primer array es: 
    <?php foreach($array1 as $num){
        echo $num." ";
    }?>
    </p>
    <p>El segundo array es:
    <?php foreach($array2 as $num){
        echo $num." ";
    }?>
    </p>
    <p>El array sumado es: 
    <?php foreach($arraysuma as $num){
        echo $num." ";
    }?>

</div>
