<?php
    //6-Crear una expresión regular que valide cualquier fecha con el siguiente formato: DD-MM-AAAA
?>

<html>
<head>
    <title>Ejercicio 6 </title>
</head>
<body>
   <?php
        $fecha = $_GET["fecha"];
        $array = explode("-",$fecha);
        $dia = $array[0];
        $mes = $array[1];
        $anyo = $array[2];
        $check = checkdate($mes,$dia,$anyo);
        if($check == 1) : 
    ?>
            <p>La fecha es correcta</p>
        <?php else : ?>
            <p>La fecha es incorrecta</p>
        <?php endif; ?>
        <a href="ejercicio6.php">Volver</a>
</body>
</html>
