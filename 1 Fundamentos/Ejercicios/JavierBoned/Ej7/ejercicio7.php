<html>
<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Ejercicio7</title>
</head>
<body>
    <div>
        Introduzca la comida a buscar: <input onkeyup="busqueda()" type="text" id="objeto" >
    </div>
    <ul class="list-group" id="lista">
    </ul>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script>

function busqueda(){
    var texto = document.getElementById("objeto").value;
    $(lista).empty();
    if(texto.length >= 2){
        var parametros = {
                "texto" : texto
        };
        $.ajax({
            dataType: 'json',
            data: parametros,
            url: 'ej7script.php',
            type: 'post',
            success:  function (response) { //una vez que el archivo recibe el request lo procesa y lo devuelve
                var content = response;
                var x = new Array(content.length);
                for (var i = 0; i < content.length; i++) {
                    $("ul").append("<li class='list-group-item'>"+content[i]["nombre"]+"</li>");
                }
            }           
        });
    }
}
</script>

</body>
</html>

